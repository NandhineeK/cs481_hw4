import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class AnimationPage extends StatefulWidget {
  @override
  _AnimationPageState createState() => _AnimationPageState();
}
class _AnimationPageState extends State<AnimationPage> 
  with SingleTickerProviderStateMixin {
  AnimationController animController;
  Animation<double> animation;

  @override
  void initState()
  {
  super.initState();
  animController=AnimationController(
  duration:Duration(seconds:5),
  vsync:this,
  );
  animation = Tween<double>(
  begin: 0,
  end: 2*math.pi,
  ).animate(animController)
  ..addListener((){
  setState((){});
  })
  ..addStatusListener((status) {
  if (status == AnimationStatus.completed) {
  animController.reverse();
  } else if (status == AnimationStatus.dismissed) {
  animController.forward();
  }
  });
  animController.forward();
  }

  @override
  Widget build(BuildContext context) {
  return Scaffold(
  backgroundColor: Colors.black,
  body: Stack(
  children:<Widget>[
    Padding(
      padding:EdgeInsets.fromLTRB(115,600,100,0),
      child:Text(
        'Welcome, click above icon!!',textAlign: TextAlign.center,
        style: TextStyle(
          color:Colors.yellow,
        ),
      ),
    ),

    RaisedButton(
      padding: EdgeInsets.fromLTRB(180,550,100,0),
      child: Icon(Icons.skip_next, color: Colors.yellow,size:40),
      onPressed:(){
        Navigator.pushNamed(context,'/second');
        },
        color:Colors.black,

    ),
    Align(
  child:
  Transform.rotate(
  angle: animation.value,
  child: Container(
  alignment: Alignment.center,
  padding: EdgeInsets.all(30),
  child: Image.asset(
  'assets/disney.jpg',
  ),
  ),
  ),
    ),
    ],
  ),
  );
  }
  @override
  void dispose()
  {
  animController.dispose();
  super.dispose();
  }
  }

