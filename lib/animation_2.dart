import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main()=>runApp(MaterialApp(home: FadeTransitionSample(),));
class FadeTransitionSample extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Fade();
}

class _Fade extends State<FadeTransitionSample> with TickerProviderStateMixin {
  AnimationController animation;
  Animation<double> _fadeInFadeOut;

  @override
  void initState() {
    super.initState();
    animation = AnimationController(vsync: this, duration: Duration(seconds: 5),);
    _fadeInFadeOut = Tween<double>(begin: 0.0, end: 0.5).animate(animation);

    animation.addStatusListener((status){
      if(status == AnimationStatus.completed){
        animation.reverse();
      }
      else if(status == AnimationStatus.dismissed){
        animation.forward();
      }
    });
    animation.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children:[
            Padding(
              padding: EdgeInsets.fromLTRB(90,52,32,32),
              child: FadeTransition(
                opacity: _fadeInFadeOut,
                child: Container(
                  child: Image.asset('assets/cindrella.jpg',
                    //color: Colors.green,
                    width: 250,
                    height: 250,
                  ),
                ),
              ),
            ),
            //Padding(
            //padding: EdgeInsets.fromLTRB(20,350,20,100),
            Center(
              child:Text("Cindrella",
                style:TextStyle(
                  color:Colors.black,
                  fontFamily: 'Pacifico',
                  fontSize: 35,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20,400,20,100),
              child:Text('When her father unexpectedly dies, young Ella finds '
                  'herself at the mercy of her cruel stepmother and'
                  ' her scheming stepsisters. Never one to give up hope, '
                  'Ellas fortunes begin to change after meeting a dashing stranger.',textAlign: TextAlign.center,
                style:TextStyle(
                  color:Colors.blueAccent,
                  fontFamily: 'Pacifico',
                  fontSize: 19,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top:615,left: 230),
              child:Text('Homepage'),
            ),
            Padding(
              padding: EdgeInsets.only(top:600,left: 300),
              child:IconButton(
                icon:Icon(Icons.home,
                  color: Colors.blueGrey,),
                onPressed:(){
                  Navigator.pop(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

